# CCPWGL Ship Loader

Files in `lib/` copied straight from [ccpwgl](https://github.com/ccpgames/ccpwgl).

## Usage
- Canvas size must be set using the canvas elements width and height attributes.
- `rotationX` and `rotationY` set the camera angle.
- `defaultScene` loads the background, I don't know how to find a list of alternative scenes.
- `camera.focus` must be called once the ship has finished loading hence the setInterval.
- The second parameter that gets passed to `camera.focus` is a distance multiplier that is based on the size of the ship so ships should always be in view when loaded.
