
// Settings.
const defaultScene = 'res:/dx9/scene/universe/m10_cube.red';
const rotationX = -0.6;
const rotationY = 0.3;

// Create the scene and camera.
const canvas = document.getElementById('canvas');
ccpwgl.initialize(canvas, {});
const scene = ccpwgl.loadScene(defaultScene);
const camera = ccpwgl.createCamera(canvas, {rotationX, rotationY}, true);

// Loads a ship and returns it when loading is completed.
function loadShip(dna) {
  return new Promise((resolve) => {
    const ship = scene.loadShip(dna);
    const intervalId = setInterval(() => {
      if (ship.isLoaded()) {
        clearInterval(intervalId);
        resolve(ship);
      }
    }, 100);
  });
}

async function init() {
  // const ship = await loadShip('at1_t1:amarrbase:amarr'); // Amarr Titan
  const ship = await loadShip('mf4_t1:minmatarbase:minmatar'); // Rifter
  camera.focus(ship, 3);
}

init();
